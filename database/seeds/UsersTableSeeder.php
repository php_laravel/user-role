<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::truncate();
        DB::table('role_user')->truncate();


        $adminRole  = Role::where('name', 'admin')->first();
        $authorRole = Role::where('name', 'author')->first();
        $userRole   = Role::where('name', 'user')->first();

        $admin = User::create([
            'name'      =>  'Admin',
            'email'     =>  'admin@email.com',
            'password'  =>  bcrypt('admin')
        ]);

        $author = User::create([
            'name'      =>  'Author',
            'email'     =>  'author@email.com',
            'password'  =>  bcrypt('author')
        ]);

        $user = User::create([
            'name'      =>  'User',
            'email'     =>  'user@email.com',
            'password'  =>  bcrypt('user')
        ]);

        $admin->roles()->attach($adminRole);
        $author->roles()->attach($authorRole);
        $user->roles()->attach($userRole);

    }
}
